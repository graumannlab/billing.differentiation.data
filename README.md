## Project ##

R package billing.differentiation.data contains the omics data from Anja Billing's stem cell differentiation project.

## Install ##

    devtools::install_bitbucket('graumannlab/billing.differentiation.data', auth_user = 'xxx', password = 'xxx')

## Original data ##

The installed package contains the original data files:

    # Proteomics: sample design 
    system.file('extdata/maxquant/sample_design.txt', package = 'billing.differentiation.data')

    # Proteomics: protein groups, generated with Max Quant from MS spectra
    system.file('extdata/maxquant/combined/txt/proteinGroups.txt', package = 'billing.differentiation.data')

    # Proteomics: phosphosites, generated with Max Quant from MS spectra
    system.file('extdata/maxquant/combined/txt/Phospho (STY)Sites.txt', package = 'billing.differentiation.data')

    # rnaseq: gene counts, generated with A*/rsubread
    system.file('extdata/rnaseq/gene_counts.txt', package = 'billing.differentiation.data')

## Ready-to-use esets ##

It also contains ready-to-use esets, each with

   * a sample variable 'subgroup' in the pdata defining the design
   * useful feature variables in the fdata.

```
#!R
    billing.differentiation.data::billing.diff.protein.ratios
    billing.differentiation.data::billing.diff.phospho.ratios
    billing.differentiation.data::billing.diff.phospho.occupancies
    billing.differentiation.data::rna.voomcounts
```

## Contrasts ##

The package also contains a contrast vector, which defines the subgroup contrasts of interest:

```
#!R

   billing.differentiation.data::contrasts
```