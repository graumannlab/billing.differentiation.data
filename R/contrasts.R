#' Billing differentiation: relevant contrasts
#' @export
contrasts <- c(EM0.8_0  = 'EM0.8-EM0.0',   # contrasts <- c(EM30_0 = 'EM30-EM0.0')
               EM01_0   = 'EM01-EM0.0',
               EM01_0.8 = 'EM01-EM0.8',
               EM02_0   = 'EM02-EM0.0',
               EM02_01  = 'EM02-EM01',
               EM05_0   = 'EM05-EM0.0',
               EM05_02  = 'EM05-EM02',
               EM15_0   = 'EM15-EM0.0',
               EM15_05  = 'EM15-EM05',
               EM30_0   = 'EM30-EM0.0',
               EM30_15  = 'EM30-EM15',
               EM30_BM  = 'EM30-BM0.0', # EM0EM01 + EM02 + EM
               BM_EM0   = 'BM0.0-EM0.0'
)
